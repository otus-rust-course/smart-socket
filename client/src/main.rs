use std::net::{TcpStream, Shutdown};
use std::io::{self, prelude::*};
use std::process;
use std::str::from_utf8;

fn close_app() {
    println!("Goodbue!");
    process::exit(0);
}

fn get_msg() -> String {
    println!("Command list:\n\t1. On\\off socket\n\t2. Get socket's info\n\t3. Exit");
    let mut msg = String::new();
    let _ = io::stdin().read_line(&mut msg);
    let msg = msg.trim();

    

    match msg {
        "1" | "turn" => "turn".to_string(),
        "2" | "info" => "info".to_string(),
        "3" | "exit" => "".to_string(),
        _ => "".to_string(),
    }
}

fn send_msg(stream: &mut TcpStream) {
    let msg = get_msg();

    match msg.as_str() {
        "turn" | "info" => {
            let msg = msg.trim().as_bytes();
            stream.write_all(msg).unwrap();
            stream.shutdown(Shutdown::Write).unwrap();
        },
        "" => close_app(),
        _ => close_app()
    }
}

fn print_response(stream: &mut TcpStream) {
    let mut buffer = [0; 1024];
    stream.read_exact(&mut buffer).unwrap();
    let response = from_utf8(&buffer).unwrap();
    println!("\nResponse: {}\n", response);
}

fn main() {
    match TcpStream::connect("localhost:3000") {
        Ok(mut stream) => {
            println!("Successfully connected to server in port 3000");

            send_msg(&mut stream);
            print_response(&mut stream);
        },
        Err(e) => {
            println!("Failed to connect: {}", e);
        }
    }
}