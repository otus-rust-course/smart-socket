#[derive(Debug, PartialEq, Clone)]
pub enum SocketStateType {
  ON,
  OFF,
}

#[derive(Debug, PartialEq, Clone)]
pub struct Socket {
  pub name: String,
  pub state: SocketStateType,
  pub info: String,
}

impl Socket {
  pub fn new(name: String, info: String) -> Socket {
    Socket {
      name,
      state: SocketStateType::OFF,
      info,
    }
  }

  pub fn on(&mut self) {
    self.state = SocketStateType::ON
  }

  pub fn off(&mut self) {
    self.state = SocketStateType::OFF
  }

  pub fn turn(&mut self) {
    println!("\n\n{:?}\n\n", self.state);
    if self.state == SocketStateType::ON {
      self.off()
    } else {
      println!("123");
      self.on()
    }
  }

  pub fn get_info(&self) -> String {
    if self.state == SocketStateType::ON {
        format!("{}:\n\t{}", self.name, self.info)
    } else {
        format!(
            "{} is turned off. If you want a description, you should enable the socket",
            self.name
        )
    }
  }
}

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn create_new_socket() {
    let name = "socket".to_string();
    let info = "Some info".to_string();
    let socket = Socket {
      name: name.clone(),
      info: info.clone(),
      state: SocketStateType::OFF,
    };

    assert_eq!(socket, Socket::new(name, info));
  }
}