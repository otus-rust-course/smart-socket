use chrono::Utc;
use server::Socket;
use std::thread;
use std::net::{TcpListener, TcpStream};
use std::io::{self, BufReader, BufRead, Write};

fn handle_client(mut stream: TcpStream, mut socket: Socket) -> io::Result<()> {
    let buffer = BufReader::new(&mut stream);
    let request = buffer.lines().next().unwrap().unwrap();

    println!("[{:?}]: NEW REQUEST. Get point \"{}\"", Utc::now(), request);

    match request.as_str() {
        "turn" | "1" => {
            socket.turn();
            let response = b"Socket is turned";
            stream.write_all(response).unwrap();
            stream.flush().unwrap();
        }
        "info" | "2" => {
            let response = socket.get_info();
            stream.write_all(response.as_bytes()).unwrap();
            stream.flush().unwrap();
        }
        _ => println!("Something wrong!")
    }

    Ok(())
}

fn main() {
    let socket= Socket::new("".to_string(), "".to_string());

    let listener = TcpListener::bind("0.0.0.0:3000").unwrap();
    println!("Server listening on port 3000");
    listener.incoming().for_each(|stream| match stream {
        Ok(stream) => {
            println!(
                "[{}]: NEW CONNECTION from {}",
                Utc::now(),
                stream.peer_addr().unwrap(),
            );
            let socket = socket.clone();
            thread::spawn(move || handle_client(stream, socket));
        }
        Err(e) => println!("Error: {}", e)
    });
}